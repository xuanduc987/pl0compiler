﻿namespace PL0Compiler
{
    /// <summary>
    /// Lexical Analyser for PL/0 Compiler
    /// </summary>
    internal class LexicalAnalyser
    {
        #region /---------- Fields ----------/

        /// <summary>
        /// The max number
        /// </summary>
        private const int MaxNumber = 999999;

        /// <summary>
        /// The max length of ident 
        /// </summary>
        private const int MaxLength = 10;

        /// <summary>
        /// The document to analyse
        /// </summary>
        private readonly string _document;

        /// <summary>
        /// The current index in document string
        /// </summary>
        private int _index;

        /// <summary>
        /// The last read character
        /// </summary>
        private char _lastchar;

        /// <summary>
        /// The value of last ident
        /// </summary>
        private string _value;

        /// <summary>
        /// The value of last number
        /// </summary>
        private int _number;

        /// <summary>
        /// The last error
        /// </summary>
        private string _error;

        #endregion

        #region /---------- Constructors ----------/

        /// <summary>
        /// Initializes a new instance of the <see cref="LexicalAnalyser"/> class.
        /// </summary>
        /// <param name="document">The document need analyse.</param>
        public LexicalAnalyser(string document)
        {
            Line = 1;
            _document = document;
            _lastchar = ' ';
        }
        #endregion

        #region /---------- Properties ----------/

        /// <summary>
        /// The current column position
        /// </summary>
        public int Col { get; private set; }

        /// <summary>
        /// The  current line number
        /// </summary>
        public int Line { get; private set; }


        #endregion

        #region /---------- Public Methods ----------/

        /// <summary>
        /// Gets the next token.
        /// </summary>
        /// <returns></returns>
        public TokenType GetNextToken()
        {
            _value = string.Empty;
            _number = -1;

            // Blank character => skip
            while (_lastchar == ' ' || _lastchar == '\n' || _lastchar == '\t' || _lastchar == '\r')
            {
                ReadNextChar();
            }

            // Ident or Keywork is started with a character
            if (char.IsLetter(_lastchar))
            {
                _value += _lastchar;
                ReadNextChar();

                while (char.IsLetterOrDigit(_lastchar))
                {
                    _value += _lastchar;
                    ReadNextChar();
                }

                FormatValue();
                return GetTokenFromIdentValue();
            }

            // A number is started with a digit
            if (char.IsDigit(_lastchar))
            {
                while (char.IsDigit(_lastchar))
                {
                    _value += _lastchar;
                    ReadNextChar();
                }

                GetNumberValue();
                if (_number > -1)
                {
                    return TokenType.Number;
                }

                _error = "Number is too big at line " + Line + ", col " + Col;
                return TokenType.None;
            }

            if (_lastchar == '(')
            {
                ReadNextChar();
                if (_lastchar == '*')
                {
                    while (true)
                    {
                        ReadNextChar();
                        if (_lastchar == char.MaxValue)
                        {
                            ReadNextChar();
                            _error = "The document is not finished at line " + Line + ", col " + Col;
                            return TokenType.None;
                        }

                        if (_lastchar == '*')
                        {
                            ReadNextChar();
                            if (_lastchar == ')')
                            {
                                ReadNextChar();
                                return GetNextToken();
                            }

                            _value = _value + '*' + _lastchar;
                        }

                        _value += _lastchar;
                    }
                }

                return TokenType.LParen;
            }

            switch (_lastchar)
            {
                case '+':
                    ReadNextChar();
                    return TokenType.Plus;
                case '-':
                    ReadNextChar();
                    return TokenType.Minus;
                case '=':
                    ReadNextChar();
                    return TokenType.Eql;
                case ')':
                    ReadNextChar();
                    return TokenType.RParen;
                case '[':
                    ReadNextChar();
                    return TokenType.LBracket;
                case ']':
                    ReadNextChar();
                    return TokenType.RBracket;
                case '*':
                    ReadNextChar();
                    return TokenType.Star;
                case '.':
                    ReadNextChar();
                    return TokenType.Period;
                case '%':
                    ReadNextChar();
                    return TokenType.Percent;
                case '/':
                    ReadNextChar();
                    return TokenType.Slash;
                case '>':
                    ReadNextChar();
                    if (_lastchar == '=')
                    {
                        ReadNextChar();
                        return TokenType.Geq;
                    }

                    return TokenType.Gtr;
                case '<':
                    ReadNextChar();
                    if (_lastchar == '=')
                    {
                        ReadNextChar();
                        return TokenType.Leq;
                    }

                    if (_lastchar == '>')
                    {
                        ReadNextChar();
                        return TokenType.Neq;
                    }

                    return TokenType.Lss;
                case ':':
                    ReadNextChar();
                    if (_lastchar != '=')
                    {
                        _error = "Expecting '=' at line " + Line + ", col " + Col;
                        return TokenType.None;
                    }

                    ReadNextChar();
                    return TokenType.Assign;
                case ',':
                    ReadNextChar();
                    return TokenType.Comma;
                case ';':
                    ReadNextChar();
                    return TokenType.Semicolon;
            }

            ReadNextChar();
            if (_lastchar == char.MaxValue)
            {
                return TokenType.Eof;
            }

            _error = "Cannot recognize character has index " + (int)_lastchar + " at line " + Line + ", col " + Col;
            return TokenType.None;
        }

        /// <summary>
        /// Gets the last ident value.
        /// </summary>
        /// <returns></returns>
        public string GetLastIdentValue()
        {
            return _value;
        }

        /// <summary>
        /// Gets the last number value.
        /// </summary>
        /// <returns></returns>
        public int GetLastNumberValue()
        {
            return _number;
        }

        /// <summary>
        /// Gets the last error message.
        /// </summary>
        /// <returns></returns>
        public string GetLastErrorMessage()
        {
            return _error;
        }

        /// <summary>
        /// Determines whether has next token.
        /// </summary>
        /// <returns></returns>
        public bool HasNextToken()
        {
            return _index <= _document.Length;
        }

        #endregion

        #region /---------- Private Methods ----------/

        /// <summary>
        /// Read the next character.
        /// </summary>
        private void ReadNextChar()
        {
            _lastchar = _index < _document.Length ? _document[_index] : char.MaxValue;
            ++_index;
            if (_lastchar == '\n')
            {
                Col = 0;
                Line = Line + 1;
            }
            else
            {
                Col = Col + 1;
            }
        }

        /// <summary>
        /// Formats the value.
        /// </summary>
        private void FormatValue()
        {
            _value = _value.ToLower();

            if (_value.Length > MaxLength)
            {
                _value = _value.Substring(0, MaxLength);
            }
        }

        /// <summary>
        /// Gets the token from ident value, if ident value is keyword then return corresponding keyword.
        /// </summary>
        /// <returns></returns>
        private TokenType GetTokenFromIdentValue()
        {
            if (_value.ToLower().Equals("begin"))
            {
                return TokenType.KwBegin;
            }

            if (_value.ToLower().Equals("call"))
            {
                return TokenType.KwCall;
            }

            if (_value.ToLower().Equals("const"))
            {
                return TokenType.KwConst;
            }

            if (_value.ToLower().Equals("do"))
            {
                return TokenType.KwDo;
            }

            if (_value.ToLower().Equals("end"))
            {
                return TokenType.KwEnd;
            }

            if (_value.ToLower().Equals("else"))
            {
                return TokenType.KwElse;
            }

            if (_value.ToLower().Equals("for"))
            {
                return TokenType.KwFor;
            }

            if (_value.ToLower().Equals("if"))
            {
                return TokenType.KwIf;
            }

            if (_value.ToLower().Equals("odd"))
            {
                return TokenType.KwOdd;
            }

            if (_value.ToLower().Equals("procedure"))
            {
                return TokenType.KwProc;
            }

            if (_value.ToLower().Equals("program"))
            {
                return TokenType.KwProg;
            }

            if (_value.ToLower().Equals("then"))
            {
                return TokenType.KwThen;
            }

            if (_value.ToLower().Equals("to"))
            {
                return TokenType.KwTo;
            }

            if (_value.ToLower().Equals("var"))
            {
                return TokenType.KwVar;
            }

            if (_value.ToLower().Equals("while"))
            {
                return TokenType.KwWhile;
            }

            return TokenType.Ident;
        }

        /// <summary>
        /// Gets the number from string value.
        /// </summary>
        private void GetNumberValue()
        {
            int val;
            if (int.TryParse(_value, out val) && val <= MaxNumber)
            {
                _number = val;
            }
        }

        #endregion
    }
}
