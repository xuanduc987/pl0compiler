﻿using System;
using System.IO;

namespace PL0Compiler
{
    class Program
    {
        static void Main(string[] args)
        {
            string line;
            string dir = string.Empty;

            if (args.Length > 0)
            {
                dir = args[0];
            }
            else
            {
#if DEBUG
                Console.WriteLine("Enter file name.");
                dir = Console.ReadLine();
#else
                Console.WriteLine("Please enter file name as parameter");
#endif
            }

            try
            {
                using (var sr = new StreamReader(dir))
                {
                    line = sr.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
                return;
            }

            var syntaxAnalyser = new SyntaxAnalyser(line);
            Console.WriteLine("Result:");
            if (syntaxAnalyser.CheckSyntax())
            {
                Console.WriteLine("OK");
            }
            else
            {
                Console.WriteLine(syntaxAnalyser.Error);
            }
            Console.WriteLine();
            Console.WriteLine("Press any key to exit program");
            Console.ReadLine();
        }
    }
}
