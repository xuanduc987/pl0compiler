﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace PL0Compiler
{
    /// <summary>
    /// Syntax Analyser for PL/0 Compiler
    /// </summary>
    internal class SyntaxAnalyser
    {
        #region /---------- Fields ----------/

        /// <summary>
        /// The lexical analyser
        /// </summary>
        private LexicalAnalyser _lexicalAnalyser;

        private TokenType _lastToken;

        #endregion

        #region /---------- Constructors ----------/

        /// <summary>
        /// Initializes a new instance of the <see cref="SyntaxAnalyser"/> class.
        /// </summary>
        /// <param name="document">The document.</param>
        public SyntaxAnalyser(string document)
        {
            _lexicalAnalyser = new LexicalAnalyser(document);
        }

        #endregion

        #region /---------- Properties ----------/

        /// <summary>
        /// Gets the error.
        /// </summary>
        /// <value>
        /// The error.
        /// </value>
        public string Error { get; private set; }

        #endregion

        #region /---------- Public Methods ----------/

        /// <summary>
        /// Checks the syntax.
        /// </summary>
        /// <returns></returns>
        public bool CheckSyntax()
        {
            GetNextToken();
            try
            {
                return Program();
            }
            catch (Exception e)
            {
                Error = e.Message;
                return false;
            }
        }

        #endregion

        #region /--------- Private Methods ---------/

        /// <summary>
        /// Gets the next token.
        /// </summary>
        private void GetNextToken()
        {
            _lastToken = _lexicalAnalyser.GetNextToken();
            if (_lastToken == TokenType.None)
            {
                throw new Exception(_lexicalAnalyser.GetLastErrorMessage());
            }
        }

        /// <summary>
        /// Throws the expecting exception.
        /// </summary>
        /// <param name="tokenName">Name of the token.</param>
        /// <exception cref="System.Exception">Expecting  + tokenName +  at line  + _lexicalAnalyser.Line + , col  + _lexicalAnalyser.Col</exception>
        private void ThrowExpectingException(string tokenName)
        {
            throw new Exception("Expecting " + tokenName + " at line " + _lexicalAnalyser.Line + ", col " + _lexicalAnalyser.Col);
        }
             
        private bool Program()
        {
            if (_lastToken == TokenType.KwProg)
            {
                GetNextToken();
                if (_lastToken == TokenType.Ident)
                {
                    GetNextToken();
                    if (_lastToken == TokenType.Semicolon)
                    {
                        GetNextToken();
                        Block();
                        if (_lastToken == TokenType.Period)
                        {
                            return true;
                        }

                        ThrowExpectingException("PERIOD");
                    }
                    else
                    {
                        ThrowExpectingException("SEMICOLON");
                    }
                }
                else
                {
                    ThrowExpectingException("IDENT");
                }
            }
            else
            {
                ThrowExpectingException("PROGRAM");
            }

            return false;
        }

        private void Block()
        {
            // const x = 3, y = 5,...;
            if (_lastToken == TokenType.KwConst)    
            {
                do
                {
                    GetNextToken();
                    DeclareConst();
                }
                while (_lastToken == TokenType.Comma);

                if (_lastToken == TokenType.Semicolon)
                {
                    GetNextToken();
                }
                else
                {
                    ThrowExpectingException("SEMICOLON");
                }
            }

            // var x, y[10],...;
            if (_lastToken == TokenType.KwVar)  
            {
                do
                {
                    GetNextToken();
                    DeclareVar();
                }
                while (_lastToken == TokenType.Comma);

                if (_lastToken == TokenType.Semicolon)
                {
                    GetNextToken();
                }
                else
                {
                    ThrowExpectingException("SEMICOLON");
                }
            }

            // procedure
            while (_lastToken == TokenType.KwProc) 
            {
                GetNextToken();
                DeclareProcedure();
            }

            // main begin
            if (_lastToken == TokenType.KwBegin)    
            {
                do
                {
                    GetNextToken();
                    Statement();
                }
                while (_lastToken == TokenType.Semicolon);

                if (_lastToken != TokenType.KwEnd)
                {
                    ThrowExpectingException("END");
                }
                else
                {
                    GetNextToken();
                }
            }
            else
            {
                ThrowExpectingException("BEGIN");
            }
        }

        private void DeclareProcedure()
        {
            if (_lastToken == TokenType.Ident)
            {
                GetNextToken();
                if (_lastToken == TokenType.LParen)
                {
                    do
                    {
                        GetNextToken();
                        if (_lastToken == TokenType.KwVar)
                        {
                            GetNextToken();
                        }
                        else
                        {
                            // Do nothing
                        }

                        if (_lastToken == TokenType.Ident)
                        {
                            GetNextToken();
                        }
                        else
                        {
                            ThrowExpectingException("IDENT");
                        }
                    }
                    while (_lastToken == TokenType.Semicolon);

                    if (_lastToken == TokenType.RParen)
                    {
                        GetNextToken();
                    }
                    else
                    {
                        ThrowExpectingException("RIGHT PARENTHESIS");
                    }
                }
                else
                {
                    // Do nothing
                }

                if (_lastToken == TokenType.Semicolon)
                {
                    GetNextToken();
                    Block();
                    if (_lastToken == TokenType.Semicolon)
                    {
                        GetNextToken();
                    }
                    else
                    {
                        ThrowExpectingException("SEMICOLON");
                    }
                }
                else
                {
                    ThrowExpectingException("SEMICOLON");
                }
            }
            else
            {
                ThrowExpectingException("IDENT");
            }
        }

        private void DeclareVar()
        {
            if (_lastToken == TokenType.Ident)
            {
                GetNextToken();
                if (_lastToken == TokenType.LBracket)
                {
                    GetNextToken();
                    if (_lastToken == TokenType.Number)
                    {
                        GetNextToken();
                        if (_lastToken == TokenType.RBracket)
                        {
                            GetNextToken();
                        }
                        else
                        {
                            ThrowExpectingException("RIGHT SQUARE BRACKET");
                        }
                    }
                    else
                    {
                        ThrowExpectingException("NUMBER");
                    }
                }
                else
                {
                    // Do nothing
                }
            }
            else
            {
                ThrowExpectingException("IDENT");
            }
        }

        private void DeclareConst()
        {
            if (_lastToken == TokenType.Ident)
            {
                GetNextToken();
                if (_lastToken == TokenType.Eql)
                {
                    GetNextToken();
                    if (_lastToken != TokenType.Number)
                    {
                        ThrowExpectingException("NUMBER");
                    }
                    else
                    {
                        GetNextToken();
                    }
                }
                else
                {
                    ThrowExpectingException("EQUAL SIGN");
                }
            }
            else
            {
                ThrowExpectingException("IDENT");
            }
        }

        private void Statement()
        {
            switch (_lastToken)
            {
                case TokenType.Ident:
                    GetNextToken();

                    // Array?
                    if (_lastToken == TokenType.LBracket) 
                    {
                        GetNextToken();
                        Expression();
                        if (_lastToken == TokenType.RBracket)
                        {
                            GetNextToken();
                        }
                        else
                        {
                            ThrowExpectingException("RIGHT SQUARE BRACKET");
                        }
                    }
                    else
                    {
                        // Do nothing
                    }

                    if (_lastToken == TokenType.Assign)
                    {
                        GetNextToken();
                        Expression();
                    }
                    else
                    {
                        ThrowExpectingException("ASSIGN SIGN");
                    }

                    break;

                case TokenType.KwCall:
                    GetNextToken();
                    if (_lastToken == TokenType.Ident)
                    {
                        GetNextToken();
                        if (_lastToken == TokenType.LParen)
                        {
                            do
                            {
                                GetNextToken();
                                Expression();
                            } 
                            while (_lastToken == TokenType.Comma);

                            if (_lastToken == TokenType.RParen)
                            {
                                GetNextToken();
                            }
                            else
                            {
                                ThrowExpectingException("RIGHT PARENTHESIS");
                            }
                        }
                        else
                        {
                            // Do nothing
                        }
                    }
                    else
                    {
                        ThrowExpectingException("IDENT");
                    }

                    break;

                case TokenType.KwBegin:
                    do
                    {
                        GetNextToken();
                        Statement();
                    }
                    while (_lastToken == TokenType.Semicolon);

                    if (_lastToken != TokenType.KwEnd)
                    {
                        ThrowExpectingException("END");
                    }
                    else
                    {
                        GetNextToken();
                    }

                    break;

                case TokenType.KwIf:
                    GetNextToken();
                    Condition();
                    if (_lastToken == TokenType.KwThen)
                    {
                        GetNextToken();
                        Statement();
                        if (_lastToken == TokenType.KwElse)
                        {
                            GetNextToken();
                            Statement();
                        }
                        else
                        {
                            // Do nothing
                        }
                    }
                    else
                    {
                        ThrowExpectingException("THEN");
                    }

                    break;

                case TokenType.KwWhile:
                    GetNextToken();
                    Condition();
                    if (_lastToken == TokenType.KwDo)
                    {
                        GetNextToken();
                        Statement();
                    }
                    else
                    {
                        ThrowExpectingException("DO");
                    }

                    break;

                case TokenType.KwFor:
                    GetNextToken();
                    if (_lastToken == TokenType.Ident)
                    {
                        GetNextToken();
                        if (_lastToken == TokenType.Assign)
                        {
                            GetNextToken();
                            Expression();
                            if (_lastToken == TokenType.KwTo)
                            {
                                GetNextToken();
                                Expression();
                                if (_lastToken == TokenType.KwDo)
                                {
                                    GetNextToken();
                                    Statement();
                                }
                                else
                                {
                                    ThrowExpectingException("DO");
                                }
                            }
                            else
                            {
                                ThrowExpectingException("TO");
                            }
                        }
                        else
                        {
                            ThrowExpectingException("ASSIGN SIGN");
                        }
                    }
                    else
                    {
                        ThrowExpectingException("IDENT");
                    }

                    break;
            }
        }

        private void Condition()
        {
            if (_lastToken == TokenType.KwOdd)
            {
                GetNextToken();
                Expression();
            }
            else
            {
                Expression();
                if (_lastToken == TokenType.Eql || _lastToken == TokenType.Gtr
                    || _lastToken == TokenType.Geq || _lastToken == TokenType.Lss
                    || _lastToken == TokenType.Leq || _lastToken == TokenType.Neq)
                {
                    GetNextToken();
                    Expression();
                }
                else
                {
                    ThrowExpectingException("RELOP");
                }
            }
        }

        private void Expression()
        {
            if (_lastToken == TokenType.Plus || _lastToken == TokenType.Minus)
            {
                GetNextToken();
            }
            else
            {
                // Do nothing
            }
            
            Term();
            while (_lastToken == TokenType.Plus || _lastToken == TokenType.Minus)
            {
                GetNextToken();
                Term();
            }
        }

        private void Term()
        {
            Factor();
            while (_lastToken == TokenType.Star || _lastToken == TokenType.Slash || _lastToken == TokenType.Percent)
            {
                GetNextToken();
                Factor();
            }
        }

        private void Factor()
        {
            switch (_lastToken)
            {
                case TokenType.Ident:  
                    GetNextToken();

                    // array
                    if (_lastToken == TokenType.LBracket) 
                    {
                        GetNextToken();
                        Expression();
                        if (_lastToken != TokenType.RBracket)
                        {
                            ThrowExpectingException("RIGHT SQUARE BRACKET");
                        }
                        else
                        {
                            GetNextToken();
                        }
                    }
                    else
                    {
                        // Do nothing
                    }

                    break;

                case TokenType.Number:
                    GetNextToken();
                    break;

                case TokenType.LParen:
                    GetNextToken();
                    Expression();
                    if (_lastToken == TokenType.RParen)
                    {
                        GetNextToken();
                    }
                    else
                    {
                        ThrowExpectingException("RIGHT PARENTHESIS");
                    }

                    break;
                
                default:
                    ThrowExpectingException("FACTOR");
                    break;
            }
        }

        #endregion
    }
}
