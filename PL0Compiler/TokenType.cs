﻿namespace PL0Compiler
{
    /// <summary>
    /// Define type for Token
    /// </summary>
    internal enum TokenType
    {
        /// <summary>
        /// The none
        /// </summary>
        None,

        /// <summary>
        /// The ident
        /// </summary>
        Ident,

        /// <summary>
        /// The number
        /// </summary>
        Number,

        /// <summary>
        /// The plus sign
        /// </summary>
        Plus,

        /// <summary>
        /// The minus sign
        /// </summary>
        Minus,

        /// <summary>
        /// The star sign
        /// </summary>
        Star,

        /// <summary>
        /// The slash sign
        /// </summary>
        Slash,

        /// <summary>
        /// The percent sign
        /// </summary>
        Percent,

        /// <summary>
        /// The left parentheses
        /// </summary>
        LParen,

        /// <summary>
        /// The right parentheses
        /// </summary>
        RParen,

        /// <summary>
        /// The left square bracket
        /// </summary>
        LBracket,

        /// <summary>
        /// The right square bracket 
        /// </summary>
        RBracket,

        /// <summary>
        /// The greater sign 
        /// </summary>
        Gtr,

        /// <summary>
        /// The less sign
        /// </summary>
        Lss,

        /// <summary>
        /// The equal sign
        /// </summary>
        Eql,

        /// <summary>
        /// The greater or equal 
        /// </summary>
        Geq,

        /// <summary>
        /// The less or equal 
        /// </summary>
        Leq,

        /// <summary>
        /// The not equal
        /// </summary>
        Neq,

        /// <summary>
        /// The assign
        /// </summary>
        Assign,

        /// <summary>
        /// The comma sign
        /// </summary>
        Comma,

        /// <summary>
        /// The period sign
        /// </summary>
        Period,

        /// <summary>
        /// The semicolon sign
        /// </summary>
        Semicolon,

        /// <summary>
        /// The keyword begin
        /// </summary>
        KwBegin,

        /// <summary>
        /// The keyword call
        /// </summary>
        KwCall,

        /// <summary>
        /// The keyword constant
        /// </summary>
        KwConst,

        /// <summary>
        /// The keyword do
        /// </summary>
        KwDo,

        /// <summary>
        /// The keyword end
        /// </summary>
        KwEnd,

        /// <summary>
        /// The keyword else
        /// </summary>
        KwElse,

        /// <summary>
        /// The keyword for
        /// </summary>
        KwFor,

        /// <summary>
        /// The keyword if
        /// </summary>
        KwIf,

        /// <summary>
        /// The keyword odd
        /// </summary>
        KwOdd,

        /// <summary>
        /// The keyword procedure
        /// </summary>
        KwProc,

        /// <summary>
        /// The keyword program
        /// </summary>
        KwProg,

        /// <summary>
        /// The keyword then
        /// </summary>
        KwThen,

        /// <summary>
        /// The keyword to
        /// </summary>
        KwTo,

        /// <summary>
        /// The keyword variable
        /// </summary>
        KwVar,

        /// <summary>
        /// The keyword while
        /// </summary>
        KwWhile,

        /// <summary>
        /// The EOF
        /// </summary>
        Eof
    }
}
